class grafana::install {
    
    $grafanaver='1.9.1'
    $packages = ['curl','graphite-web', 'python-carbon', 'python-whisper']

    package { $packages:
                ensure => 'present',
    }

    -> exec { 'curl-grafana':
        command => "/usr/bin/curl -s -o /tmp/grafana.tar.gz http://grafanarel.s3.amazonaws.com/grafana-$grafanaver.tar.gz && /bin/tar -xzf /tmp/grafana.tar.gz -C /var/www/html/ && mv /var/www/html/grafana-$grafanaver /var/www/html/grafana && rm /tmp/grafana.tar.gz",
        creates => "/var/www/html/grafana/config.sample.js",
    }

    #-> exec {'unpack-grafana':
    #    command => "/bin/tar -xzf /tmp/grafana.tar.gz -C /tmp",
    #    path    => "/sbin:/bin:/usr/sbin:/usr/bin",
    #}
}
