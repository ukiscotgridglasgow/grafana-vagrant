class grafana::config {

    file { '/usr/local/bin/inject-graphite-data.sh':
        ensure => 'present',
        owner  => 'root' ,
        group  => 'root' ,
        mode   => 0755,
        source  => "puppet:///modules/grafana/usr/local/bin/inject-graphite-data.sh",
    } ->

    file { '/usr/local/bin/fix-graphite-db.sh':
        ensure => 'present',
        owner  => 'root' ,
        group  => 'root' ,
        mode   => 0755,
        source  => "puppet:///modules/grafana/usr/local/bin/fix-graphite-db.sh",
    } ->


    file { '/var/www/html/grafana/config.js':
        ensure => 'present',
        owner  => 'root' ,
        group  => 'root' ,
        mode   => 0644,
        source  => "puppet:///modules/grafana/var/www/html/grafana/config.js",
    } ->

    file { '/etc/httpd/conf.d/grafana.conf':
        ensure => 'present',
        owner  => 'root' ,
        group  => 'root' ,
        mode   => 0644,
        source  => "puppet:///modules/grafana/etc/httpd/conf.d/grafana.conf",
        notify => Service['httpd'],
    } ->

    file { '/etc/httpd/conf.d/graphite-web.conf':
        ensure => 'present',
        owner  => 'root' ,
        group  => 'root' ,
        mode   => 0644,
        source  => "puppet:///modules/grafana/etc/httpd/conf.d/graphite-web.conf",
        notify => Service['httpd'],
    } ->

    exec { 'graphite-db':
        command => "/bin/echo no | /usr/lib/python2.6/site-packages/graphite/manage.py syncdb",
        creates => "/var/lib/graphite-web/graphite.db",
    } ->

    file { '/var/lib/graphite-web/graphite.db':
        ensure => 'present',
        owner  => 'apache' ,
        group  => 'apache' ,
        mode   => 0644,
        notify => Service['httpd'],
    } 

    exec { 'add-secret-key':
        command => "/bin/echo SECRET_KEY = \\'`/bin/cat /dev/urandom | /usr/bin/tr -dc 'a-zA-Z0-9' | /usr/bin/fold -w 32 | /usr/bin/head -1`\\' >> /usr/lib/python2.6/site-packages/graphite/local_settings.py",
        unless  => "/bin/grep ^SECRET /usr/lib/python2.6/site-packages/graphite/local_settings.py",
    }
}
