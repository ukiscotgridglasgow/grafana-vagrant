class grafana::service {

    service { "carbon-cache":
                ensure      => running,
                hasstatus   => true,
                hasrestart  => true,
                enable      => true,
    } ->

    service { "httpd":
                ensure      => running,
                hasstatus   => true,
                hasrestart  => true,
                enable      => true,
                require    => Class['grafana::config'],
    }

}
