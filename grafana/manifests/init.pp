class grafana {

    class {'grafana::install': } 
    -> class {'grafana::config': } 
    -> class {'grafana::service':}
}
