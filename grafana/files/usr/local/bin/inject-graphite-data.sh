#! /bin/bash

points=100

timeshiftstart=720
let timeshiftend=$timeshiftstart-$points

timestampstart=`date -d "$timeshiftstart minutes ago" +%s`
timestart=`date -d "$timeshiftstart minutes ago" +"%H:%M"`
timeend=`date -d "$timeshiftend minutes ago" +"%H:%M"`

rpm -qa | grep -q ^nc-

if [ $? -ne 0 ]; then
    yum install -y nc
else
    echo "netcat installed..."
fi

for count in `seq 1 $points`; do
    let datatime=$timestampstart+$count*60
    value=`echo $[ 20 + $[ RANDOM % 5 ]]`
    echo democluster.wn.node9001.load_one $value $datatime | nc localhost 2003
done

echo "Data injected for 'democluster.wn.node9001.load_one' from $timestart to $timeend"
