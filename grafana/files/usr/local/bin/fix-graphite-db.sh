#! /bin/bash

/bin/echo no | /usr/lib/python2.6/site-packages/graphite/manage.py syncdb

service httpd stop

if [ -f /var/lib/graphite-web/graphite.db-journal ]; then
    rm /var/lib/graphite-web/graphite.db-journal
fi

service httpd start
