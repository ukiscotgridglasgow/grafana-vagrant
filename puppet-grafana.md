# grafana puppet module

### Version 1.0

## Introduction

Although called `grafana`, this module installs both `graphite` as well as `grafana`. It is intended to be a barebones install and is configured for use with a vagrant environment, although can be adapted in a straightforward fashion for use on hardware (particularly regaring hostnames, effectively). 

Please see also www.scotgrid.ac.uk/graphite for a parallel view of this process from a non-puppet perspective

### file listing:

* grafana/files/etc/httpd/conf.d/graphite-web.conf
* grafana/files/etc/httpd/conf.d/grafana.conf
* grafana/files/usr/local/bin/fix-graphite-db.sh
* grafana/files/usr/local/bin/inject-graphite-data.sh
* grafana/files/var/www/html/grafana/config.js
* grafana/manifests/init.pp
* grafana/manifests/install.pp
* grafana/manifests/config.pp
* grafana/manifests/service.pp

## Init

The `init.pp` script runs 

* `install.pp`
* `config.pp`
* `services.pp`

each of which contains the necessary appropriate steps for graphite and grafana

## Install

1. Installs these packages for graphite:

* `graphite-web`
* `python-carbon`
* `python-whisper`

2. Downloads the grafana tarball of a given version to /tmp, untars it to /var/www/html (requiring httpd to be installed first) and renames the directory to `grafana`, before cleaning up the /tmp directory

## Config

Installs these utility scripts:

### 1. /usr/local/bin/inject-graphite-data.sh
Installs `nc` if necessary and injects dummy data

### 2. /usr/local/bin/fix-graphite-db.sh
Fixes a current problem with the puppet module where the graphite.db database is not initialised correctly.

### 3. /var/www/html/grafana/config.js
grafana config file

### 4. /etc/httpd/conf.d/grafana.conf
grafana apache configuration

### 5. /etc/httpd/conf.d/graphite-web.conf
graphite apache configuration

Executes these commands

### 1. /bin/echo no | /usr/lib/python2.6/site-packages/graphite/manage.py syncdb
Generates graphite.db

### 2. Set ownership of graphite.db
Sets ownership of graphite.db to apache:apache

## Service

Configures the following services to `hasstatus`, `hasrestart`, `enable` as `true`

### 1. carbon-cache
Graphite carbon data collector

### 2. httpd
Apache; notified to restart by changes to the http configs above

## Notes

Something is currently not quite correct in the graphite.db initialisation as in some circumstances this does not work as expected. The utility script `fix-graphite-db.sh` fixes this until a proper fix is found (this is an issue with this module, as opposed to graphite.db itself)
