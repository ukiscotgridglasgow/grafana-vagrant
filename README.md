#  grafana-vagrant

### Version 1.0

## Introduction

**grafana-vagrant** is a very basic vagrant setup to demonstrate the (included) graphite/grafana puppet module in a CentOS 6.6 environment using the chef/centos-6.6 vagrant box. It is not intended to represent an ideal setup for production use as site specifications will vary, but does allow for testing and familiarisation with graphite and grafana in a (hopefully!) simple way. 

## Installation

### 1. For use as vagrant environment

1. Install vagrant from https://www.vagrantup.com.
2. Clone repo and `cd` into the `grafana-vagrant` directory
3. Run `vagrant up`
4. The vagrant VM will start and be provisioned using the `provision.sh` script. This currently does the following things:

    * Install `puppet` and configure it to run
    * Install the epel repo
    * Shuts off the VM firewall [may be updated]
    * Adds an alias to run the grafana module for convenience
    * Applies the puppet module
    * Runs the `inject-graphite-data.sh` script to populate the graphite instance with some dummy data to work with (also installs `nc` if necessary)

### 2. For use as puppet module

1. To use only the puppet module, clone the repo and copy out the `grafana` directory appropriate.

## Usage

To test the installation:

1. Visit http://localhost:8181/render?target=randomWalk('test.metric') to confirm that the graphite install is working correctly
2. Visit http://localhost:8182/#/dashboard/file/default.json to make sure that the grafana install is working correctly.

## Next steps

This is intended as a jumping off point for working with graphite and griffin before adapting this for your own needs. There are many guides available for graphite and grafana; note from the Glasgow installation can currently be found here: www.scotgrid.ac.uk/graphite

## Troubleshooting

### Q. `vagrant up` doesn't work, left with continuous "Warning: Connection timeout. Retrying..." messages

A. I've seen this a couple of times; running `vagrant destroy` and retrying seems to work

### Q. The puppet run includes errors like "Error: Execution of '/usr/bin/yum -d 0 -e 0 -y list python-carbon' returned 1: Error: Cannot retrieve metalink for repository: epel." 

A. this may be my network but occasionally I saw this happen; once the run fails, run `vagrant provision` to retry, which has worked in my experience. In this case, you may also see...

### Q. Internal 500 error seen with http://localhost:8181/render?target=randomWalk('test.metric') (graphite) and test graph not showing in http://localhost:8182/#/dashboard/file/default.json (grafana)

A. `vagrant ssh` into the VM and run `fix-graphite-db.sh` - the puppet config didn't run properly [under investigation]