#! /bin/bash

lver=`sudo cat /etc/redhat-release | awk '{print $(NF-1)}' | cut -d. -f1`

sudo echo "Adding puppet repo"

sudo rpm -ivh http://yum.puppetlabs.com/puppetlabs-release-el-$lver.noarch.rpm
echo "installing puppet"
sudo yum install puppet epel-release -y
echo "ensure puppet service is running"
sudo puppet resource service puppet ensure=running enable=true
sudo service iptables stop
sudo chkconfig iptables off
echo alias puppet-grafana=\'puppet apply --modulepath /vagrant/ -e \"include grafana\" --verbose\' >> /root/.bashrc
puppet apply --modulepath /vagrant/ -e "include grafana" --verbose
/usr/local/bin/inject-graphite-data.sh
/usr/local/bin/fix-graphite-db.sh
